const fsp = require('fs').promises;
const path = './input/day4-data.txt';

fsp.readFile(path, 'utf8').then( (data) => {
    const part1 = [];
    const part2 = [];
    const loop = data.trim().split(/\r?\n/);
    let multiplier = Array(loop.length).fill(1);

    loop.forEach((e, index) => {
        const win = e.slice(9, 41).match(/\d+/g);
        const mine = e.slice(41).match(/\d+/g);

        const matches = mine.filter((v) => win.some((i) => i === v));
        if (matches.length) {
            part1.push(matches.map((e) => e = 1).reduce((p, _, i) => i === 0 ? 1 : p * 2));
        }

        part2.push(1);
        matches.forEach((_, i) => {
            multiplier[index + i + 1] = multiplier[index] + multiplier[index + i + 1];
        });
    });

    console.log('part 1: ', part1.reduce((p, e) => p + e, 0));
    console.log('part 2: ', part2.reduce((p, e, i) => p + (e * multiplier[i]), 0));
});
