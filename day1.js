const fsp = require('fs').promises;

const path = './input/day1-data.txt';
const nums = {
    "one": "o1e",
    "two": "t2o",
    "three": "t3e",
    "four": "f4r",
    "five": "f5e",
    "six": "s6x",
    "seven": "s7n",
    "eight": "e8t",
    "nine": "n9e"
}

fsp.readFile(path, 'utf8').then( (data) => {
    const reg = data.replace(/one|two|three|four|five|six|seven|eight|nine/g, i => nums[i]);
    const r = reg.split(/\r?\n/).filter((e) => e.length).map((e, i) => {
        const x = e.match(/\d/g);
        const y = e.replace(/one|two|three|four|five|six|seven|eight|nine/g, i => nums[i]).match(/\d/g);
        if (i === 937) console.log(i, e, y);
        return parseInt(y[0] + y[y.length - 1]);
    }).reduce((p, e) => p + e, 0);
    console.log(r);
});
