import re
from functools import reduce

class Hand:
    def __init__(self, hands=None, values=None) -> None:
        self.hand = hands
        self.value = values

    def __repr__(self):
        return repr((self.hand, self.value))

    def sortCards(self, j=11) -> int:
        r = []
        card = {
            'T' : 10,
            'J' : j,
            'Q' : 12,
            'K' : 13,
            'A' : 14
        }
        for c in self.hand:

            if c.isdigit():
                r.append(int(c))
            else:
                r.append(card[c])
        return r

    def sortHands(self, j=False) -> int:
        cards = []
        matches = []
        for card in self.hand:
            if not card in cards:
                cards.append(card)
                matches.append(len(re.findall(card, self.hand)))

        if j and 'J' in cards and len(cards) > 1:
            x = matches.pop(cards.index('J'))
            m = matches.index(max(matches))
            matches[m] = matches[m] + x

        l = len(matches)
        if l == 1:
            return 1
        elif l == 2 and 4 in matches:
            return 2
        elif l == 2 and 3 in matches:
            return 3
        elif l == 3 and 3 in matches:
            return 4
        elif l == 3 and 2 in matches:
            return 5
        elif l == 4:
            return 6
        else:
            return 7

with open('../input/day7-data.txt') as FH:
    data = [Hand(line.strip()[:5], int(line.strip()[6:])) for line in FH]

    sort_card = sorted(data, key=lambda hand: Hand.sortCards(hand))
    part1 = sorted(sort_card, key=lambda hand: Hand.sortHands(hand), reverse=True)

    sort_joker = sorted(data, key=lambda hand: Hand.sortCards(hand, 1))
    part2 = sorted(sort_joker, key=lambda hand: Hand.sortHands(hand, True), reverse=True)

    print(reduce(lambda x, e: x + (e[0] + 1) * e[1].value, enumerate(part1), 0))
    print(reduce(lambda x, e: x + (e[0] + 1) * e[1].value, enumerate(part2), 0))