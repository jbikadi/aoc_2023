from math import floor

def nextStep(x, y, board):
    result = {'x': x, 'y': y, 'px': x, 'py': y}
    if board[y - 1][x] in '|7F':
        result['y'] -= 1
    elif board[y][x + 1] in '-J7':
        result['x'] += 1
    elif board[y + 1][x] in '|JL':
        result['y'] += 1
    elif board[y][x - 1] in '-LF':
        result['x'] -= 1
    return result

def findStart(start, board):
    result = {'x': 0, 'y': 0, 'px': 0, 'py': 0}
    for y in range(len(board)):
        x = board[y].find(start)
        if x > -1:
            return nextStep(x, y, board)
    return result


with open('../input/day10-data.txt') as FH:
    data = [line.strip() for line in FH]

    part1 = 1

    # find the starting coordinates of 'S'
    start = findStart('S', data)
    
    board = list(data)
    x = start['x']
    y = start['y']
    px = start['px']
    py = start['py']

    while board[y][x] != 'S':
        if board[y][x] == '|':
            if py < y:
                py = y
                y += 1
            else:
                py = y
                y -= 1
        elif board[y][x] == '-':
            if px < x:
                px = x
                x += 1
            else:
                px = x
                x -= 1
        elif board[y][x] == 'L':
            if px > x:
                px = x
                y -= 1
            else:
                py = y
                x += 1
        elif board[y][x] == 'J':
            if px < x:
                px = x
                y -= 1
            else:
                py = y
                x -= 1
        elif board[y][x] == '7':
            if px < x:
                px = x
                y += 1
            else:
                py = y
                x -= 1
        elif board[y][x] == 'F':
            if px > x:
                px = x
                y += 1
            else:
                py = y
                x += 1
        part1 += 1

    print('part 1: ', floor(part1 / 2))