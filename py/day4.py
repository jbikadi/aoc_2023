
# def game1(c):
#     wins = c[9:40].strip().split()
#     nums = c[41:].strip().split()
#     r = 0

#     for n in nums:
#         if any(w == n for w in wins):
#             if r == 0:
#                 r = 1
#             else:
#                 r = r + r
#     return r

with open('../input/day4-data.txt') as FH:
    data = [line.strip() for line in FH]
    part1 = []
    part2 = [1] * len(data)

    for i in range(len(data)):
        wins = data[i][9:40].strip().split()
        nums = data[i][41:].strip().split()
        r = 0
        l = 0

        for n in nums:
            if any(w == n for w in wins):
                #part1
                if r == 0:
                    r = 1
                else:
                    r = r + r
                #part2
                l = l + 1
        part1.append(r)
        for x in range(l):
            part2[i + x + 1] = part2[i + x + 1] + part2[i]

    print(sum(part1))
    print(sum(part2))