from functools import reduce

with open('../input/day6-data.txt') as FH:
    data = [line.strip() for line in FH]
    #part 1
    times = [int(num) for num in data[0][10:].split()]
    dists = [int(num) for num in data[1][10:].split()]

    #part2
    time = int(''.join([num for num in data[0] if num.isdigit()]))
    dist = int(''.join([num for num in data[1] if num.isdigit()]))

    part1 = []
    part2 = 0

    for i in range(len(times)):
        t = 0

        for x in range(times[i]):
            if x * (times[i] - x) > dists[i]:
                t = t + 1
        if t > 0:
            part1.append(t)

    for x in range(time):
        if x * (time - x) > dist:
            part2 = part2 + 1

    print(reduce(lambda x, y: x * y, part1))
    print(part2)