
def game1(n):
    cubes = [hand.split(', ') for hand in n[1].split('; ')]
    rgb = {
        'red' : 12,
        'green' : 13,
        'blue' : 14
    }
    for colors in cubes:
        for color in colors:
            x = color.split()
            if rgb[x[1]] < int(x[0]):
                return 0
    return n[0] + 1

def game2(n):
    cubes = [hand.split(', ') for hand in n.split('; ')]
    total = {}
    result = 1
    for colors in cubes: 
        for color in colors:
            x = color.split()
            if total.get(x[1]) == None or total[x[1]] < int(x[0]):
                total[x[1]] = int(x[0])
    for i in total:
        result = result * total[i]
    return result

with open('../input/day2-data.txt') as FH:
    data = [line[line.find(':') + 2:].strip() for line in FH]

    part1 = sum(map(game1, enumerate(data)))
    part2 = sum(map(game2, data))

    print(part1)
    print(part2)
