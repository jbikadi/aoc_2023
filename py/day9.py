from functools import reduce

with open('../input/day9-data.txt') as FH:
    data = [line.strip() for line in FH]

    part1 = []
    part2 = []

    for line in data:
        nums = [[int(num) for num in line.split()]]
        while not all(0 == i for i in nums[-1]):
            numDiff = []
            for i in range(len(nums[-1])):
                if i > 0:
                    numDiff.append(nums[-1][i] - nums[-1][i - 1])
            nums.append(numDiff)
        part1.append(reduce(lambda x, y: x + y, map(lambda a: a[-1], nums)))
        part2.append(reduce(lambda x, y: y - x, map(lambda a: a[0], reversed(nums))))


    print('part1: ', sum(part1))
    print('part2: ', sum(part2))