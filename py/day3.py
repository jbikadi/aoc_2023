import re

def adjnums(n, s):
    r = (
        (
            s['y'] <= n['y'] + 1 and 
            s['y'] >= n['y'] - 1
        ) and 
        (
            s['x'] <= n['x'] + n['l'] and 
            s['x'] >= n['x'] - 1
        )
    )
    return r

with open('../input/day3-data.txt') as FH:
    data = [line.strip() for line in FH]

    # x, y
    symbols = []
    # x, y, number, length
    numbers = []
    # x, y
    stars = []
    part1 = []
    part2 = []

    for y in range(len(data)):
        nums = re.findall(r"\d+", data[y])
        i = 0

        for num in nums:
            x = data[y].find(num, i)
            if x > -1:
                numbers.append({'x':x, 'y':y, 'n':int(num), 'l':len(num)})
                i = x + len(num)

        for x in range(len(data[y])):
            if not data[y][x].isdigit() and data[y][x] != '.':
                symbols.append({'x':x, 'y':y})
            if data[y][x] == '*':
                stars.append({'x':x, 'y':y})


    for n in numbers:
        if any(adjnums(n, s) for s in symbols):
            part1.append(n['n'])

    for s in stars:
        gear = [n['n'] for n in numbers if adjnums(n, s)]
        if len(gear) == 2:
            part2.append(gear[0] * gear[1])


    print(sum(part1))
    print(sum(part2))