from functools import reduce

data = open('../input/day1-data.txt').read().strip().split()

numbers = {
    "one" : "o1e",
    "two" : "t2o",
    "three" : "t3e",
    "four" : "f4r",
    "five" : "f5e",
    "six" : "s6x",
    "seven" : "s7n",
    "eight" : "e8t",
    "nine" : "n9e",
    "zero" : "z0o"
}

def parseInts(n):
    result = [c for c in n if c.isdigit()]
    return int(result[0] + result[-1])

def replaceNums(n):
    s = n
    for key, value in numbers.items():
        s = s.replace(key, value)
    result = [c for c in s if c.isdigit()]
    return int(result[0] + result[-1])

part1 = reduce(lambda x, y: x + y, list(map(parseInts, data)))
part2 = reduce(lambda x, y: x + y, list(map(replaceNums, data)))

print(part1)
print(part2)