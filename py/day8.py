from math import lcm

def findMultiple(a):
    b = list(a)
    highest = max(b)
    n = len(b)
    g = len(set(b))
    while g > 1:
        for i in range(n):
            while b[i] < highest:
                b[i] += a[i]
        highest = max(b)
        g = len(set(b))
    return highest

with open('../input/day8-data.txt') as FH:
    data = [line.strip() for line in FH if len(line.strip()) > 0]

    instuctions = list(data[0])
    keys = {k[:3] : {'L': k[7:10], 'R':k[12:15]} for k in data[1:]}

    # part 1
    n = len(instuctions)
    step = "AAA"
    part1 = 0

    # part 2
    part2 = []
    a = [k for k in list(keys) if k[2] == 'A']

    while step != "ZZZ":
        step = keys[step][instuctions[part1 % n]]
        part1 += 1

    for z in a:
        steps = z
        i = 0
        while steps[2] != 'Z':
            steps = keys[steps][instuctions[i % n]]
            i += 1
        part2.append(i)

    print('start: ', part2)

    print('part 1: ', part1)
    print('part 2: ', lcm(*part2))