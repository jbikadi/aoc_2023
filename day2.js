const fsp = require('fs').promises;
const path = './input/day2-data.txt';

fsp.readFile(path, 'utf8').then( (data) => {
    const r = data.split(/\r?\n/).filter(e => e.length).map((e, i) =>{
        const id = e.match(/\d+/);
        const rgb = e.match(/\d+\s(red|green|blue)/g).map((v) => {
            const result = v.split(' ');
            return {[result[1]] : parseInt(result[0])}
        });

        const red = rgb.filter((v) => 'red' in v).reduce((p, v) => p && v.red <= 12, true);
        const green = rgb.filter((v) => 'green' in v).reduce((p, v) => p && v.green <= 13, true);
        const blue = rgb.filter((v) => 'blue' in v).reduce((p, v) => p && v.blue <= 14, true);

        if (red && green && blue) {
            return parseInt(id[0]);
        }
        return 0;
    }).reduce((p, e) => p + e, 0);
    console.log('part 1: ', r);
});

fsp.readFile(path, 'utf8').then( (data) => {
    const r = data.split(/\r?\n/).filter(e => e.length).map((e, i) =>{
        const rgb = e.match(/\d+\s(red|green|blue)/g).map((v) => {
            const result = v.split(' ');
            return {[result[1]] : parseInt(result[0])}
        });

        const red = rgb.filter((v) => 'red' in v).reduce((p, v) => p < v.red ? (v.red) : p, 0);
        const green = rgb.filter((v) => 'green' in v).reduce((p, v) => p < v.green ? (v.green) : p, 0);
        const blue = rgb.filter((v) => 'blue' in v).reduce((p, v) => p < v.blue ? (v.blue) : p, 0);

        return red * green * blue;
    }).reduce((p, e) => p + e, 0);
    console.log('part 2: ', r);
});