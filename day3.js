const fsp = require('fs').promises;
const path = './input/day3-data.txt';

fsp.readFile(path, 'utf8').then( (data) => {
    const part1 = [];
    const part2 = [];
    const nums = [];
    const char = [];
    const gears = []
    const loop = data.trim().split(/\r?\n/);

    loop.forEach((e, y) => {
        let x = 0;
        while (x > -1) {
            const part = e.slice(x).search(/[^\.\d]/);
            if (part > -1) {
                char.push({y, x: x + part});
                x = x + part + 1;
            } else {
                x = -1
            }
        }

        x = 0;
        while (x > -1 ) {
            const num = e.slice(x).match(/\d+/);
            if (num !== null) {
                nums.push({value: num[0], x: x + num.index, y});
                x = x + num.index + num[0].length;
            } else {
                x = -1;
            }
        }

        x = 0;
        while (x > -1) {
            const ratio = e.slice(x).search(/\*/);
            if (ratio > -1) {
                gears.push({x: x + ratio, y});
                x = x + ratio + 1;
            } else {
                x = -1;
            }
        }
    });

    // part 1
    nums.forEach((e) => {
        if (char.some((v) => (v.y >= e.y - 1 && v.y <= e.y + 1) && (v.x >= e.x - 1 && v.x <= e.x + e.value.length))) {
            part1.push(parseInt(e.value));
        }
    });

    // part 2
    gears.forEach((e) => {
        const ratio = nums.filter((n) =>
            (n.y >= e.y - 1 && n.y <= e.y + 1) && (n.x + n.value.length >= e.x && n.x <= e.x + 1)
        );
        if (ratio.length === 2) {
            part2.push(parseInt(ratio[0].value) * parseInt(ratio[1].value));
        }
    });

    console.log('part 1: ', part1.reduce((p, e) => p + e));
    console.log('part 2: ', part2.reduce((p, e) => p + e));
});
